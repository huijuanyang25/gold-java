package com.tw.questionMedium;

import java.util.*;
import java.util.stream.Collectors;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addAll(Book ...books) {
        this.books.addAll(Arrays.asList(books));
    }

    /**
     * Find all the books which contains one of the specified tags.
     *
     * @param tags The tag list.
     * @return The books which contains one of the specified tags. The returned books
     *   should be ordered by their ISBN number.
     */
    public List<Book> findBooksByTag(String ...tags) {
        // TODO:
        //   Please implement the method
        // <-start-
        List<Book> returnedBooks = new ArrayList<>();
        List<String> inputTags = Arrays.asList(tags);

        if (inputTags == null) {
            returnedBooks.add(null);
        }

        for (int i = 0; i < books.size(); i++) {
            for (int j = 0; j < inputTags.size(); j++) {
                if (books.get(i).getTags().contains(inputTags.get(j))) {
                    returnedBooks.add(books.get(i));
                }
            }
        }
        List<Book> returnedDeduplicateBooks = removeDeduplicateResults(returnedBooks);
        List<Book> sortedReturnedBooks = returnedDeduplicateBooks.stream().sorted(Comparator.comparing(Book::getIsbn)).collect(Collectors.toList());
        return sortedReturnedBooks;
        // --end-->
    }


    // TODO:
    //   You can add additional methods here if you want.
    // <-start-
    private List<Book> removeDeduplicateResults(List<Book> inputResults) {
        List<Book> removeDeduplicateResults = new ArrayList<>();
        for (int i = 0; i < inputResults.size(); i++) {
            if (!removeDeduplicateResults.contains(inputResults.get(i))) {
                removeDeduplicateResults.add(inputResults.get(i));
            }
        }
        return removeDeduplicateResults;
    }
    // --end-->
}
