package com.tw.questionEasy;

public class HtmlEscaper {
    // TODO:
    //   You can add additional members or blocks of code here if you want.
    // <-start-
    
    // --end-->

    /**
     * This function will try escaping characters according to the rules defined in HTML 4.01
     * The rules are as follows:
     *
     * (1) Every `"` character will be escaped to `&quot;`
     * (2) Every `'` character will be escaped to `&#39;`
     * (3) Every `&` character will be escaped to `&amp;`
     * (4) Every `<` character will be escaped to `&lt;`
     * (5) Every `>` character will be escaped to `&gt;`
     *
     * @param text The text to escape.
     * @return The escaped string.
     */
    public static String escape(String text) {
        // TODO:
        //   Please implement the method
        // <-start-
        if (text == null) {
            throw new IllegalArgumentException();
        }

        StringBuilder escapedText = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char eachText = text.charAt(i);
            if (eachText == '"') {
                escapedText.append("&quot;");
            } else if (eachText == '\'') {
                escapedText.append("&#39;");
            } else if (eachText == '&') {
                escapedText.append("&amp;");
            } else if (eachText == '<') {
                escapedText.append("&lt;");
            } else if (eachText == '>') {
                escapedText.append("&gt;");
            } else {
                escapedText.append(eachText);
            }
        }
        return escapedText.toString();
        // --end-->
    }
}
